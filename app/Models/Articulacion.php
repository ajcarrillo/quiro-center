<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Articulacion extends Model
{
    protected $table   = 'articulaciones';
    protected $guarded = [];
}

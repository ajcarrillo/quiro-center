<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/29/20
 * Time: 5:18 p. m.
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Entidad extends Model
{
    protected $table      = 'geo_entidades';
    protected $primaryKey = 'entidad_id';
}

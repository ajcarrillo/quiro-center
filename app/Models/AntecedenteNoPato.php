<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AntecedenteNoPato extends Model
{
    protected $table   = 'antecedentes_no_patologicos';
    protected $guarded = [];
}

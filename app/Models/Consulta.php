<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Consulta extends Model
{
    protected $table    = 'consultas';
    protected $fillable = [
        'consulta_padre_id',
        'expediente_id',
        'talla',
        'frecuencia_cardiaca',
        'frecuencia_respiratoria',
        'temperatura',
        'peso',
        'cintura',
        'motivo_consulta',
        'marcha',
        'valoracion_neurologica',
        'pruebas_especiales',
        'dx',
        'fecha',
        'medico_tratante',
        'dx_envio',
        'padecimiento_actual',
    ];

    public function expediente()
    {
        return $this->belongsTo(Expediente::class, 'expediente_id');
    }
}

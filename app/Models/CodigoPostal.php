<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/29/20
 * Time: 5:17 p. m.
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CodigoPostal extends Model
{
    protected $table   = 'codigos_postales';
    protected $appends = [ 'municipio' ];

    public function entidad()
    {
        return $this->belongsTo(Entidad::class, 'entidad_id');
    }

    public function setMunicipioAttribute()
    {
        return Municipio::query()->where('municipio_id', $this->municipio_id)
            ->first();
    }

    public function localidad()
    {
        return $this->belongsTo(Localidad::class, 'd_ciudad', 'descripcion')
            ->where('entidad_id', $this->entidad_id)
            ->where('municipio_id', $this->municipio_id);
    }
}

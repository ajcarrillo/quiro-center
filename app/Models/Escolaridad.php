<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/30/20
 * Time: 12:47 p. m.
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Escolaridad extends Model
{
    public    $incrementing = false;
    protected $table        = 'escolaridades';
    protected $primaryKey   = 'descripcion';
    protected $guarded      = [];
}

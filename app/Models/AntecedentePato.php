<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AntecedentePato extends Model
{
    protected $table   = 'antecedentes_patologicos';
    protected $guarded = [];
}

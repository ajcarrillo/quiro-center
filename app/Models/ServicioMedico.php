<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/30/20
 * Time: 12:48 p. m.
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ServicioMedico extends Model
{
    public    $incrementing = false;
    protected $table        = 'servicios_medicos';
    protected $primaryKey   = 'descripcion';
    protected $guarded      = [];
}

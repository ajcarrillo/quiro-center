<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/25/20
 * Time: 12:30 a. m.
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ExpedienteAntecedenteNoPatologico extends Model
{
    protected $table    = 'expediente_antecedentes_no_pato';
    protected $fillable = [
        'expediente_id',
        'antecedente_id',
        'observaciones',
    ];

    public function expediente()
    {
        return $this->belongsTo(Expediente::class, 'expediente_id');
    }

    public function antecedente()
    {
        return $this->belongsTo(AntecedenteNoPato::class, 'antecedente_id');
    }
}

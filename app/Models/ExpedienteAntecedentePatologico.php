<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExpedienteAntecedentePatologico extends Model
{
    protected $table    = 'expediente_antecedentes_pato';
    protected $fillable = [
        'expediente_id',
        'antecedente_id',
        'observaciones',
    ];

    public function expediente()
    {
        return $this->belongsTo(Expediente::class, 'expediente_id');
    }

    public function antecedente()
    {
        return $this->belongsTo(AntecedentePato::class, 'antecedente_id');
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/29/20
 * Time: 9:29 p. m.
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Domicilio extends Model
{
    protected $table    = 'domicilios';
    protected $fillable = [
        'addressable_type',
        'addressable_id',
        'codigo_postal_id',
        'entidad_id',
        'municipio_id',
        'localidad_id',
        'colonia',
        'calle',
        'numero',
        'cruzamiento_1',
        'cruzamiento_2',
    ];
    protected $appends  = [
        'municipio',
        'localidad',
    ];

    public function addressable()
    {
        return $this->morphTo();
    }

    public function entidad()
    {
        return $this->belongsTo(Entidad::class, 'entidad_id', 'entidad_id');
    }

    public function getMunicipioAttribute()
    {
        return Municipio::query()
            ->where('entidad_id', $this->entidad_id)
            ->where('municipio_id', $this->municipio_id)
            ->first();
    }

    public function getLocalidadAttribute()
    {
        return Localidad::query()
            ->where('entidad_id', $this->entidad_id)
            ->where('municipio_id', $this->municipio_id)
            ->where('localidad_id', $this->localidad_id)
            ->first();
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/26/20
 * Time: 12:11 a. m.
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class AntecedenteFamiliar extends Model
{
    protected $table    = 'antecedentes_heredo_familiares';
    protected $fillable = [
        'descripcion',
    ];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Expediente extends Model
{
    protected $table    = 'expedientes';
    protected $fillable = [
        'paciente_id',
        'higiene',
        'alimentacion',
        'observaciones',
    ];

    public function consultas()
    {
        return $this->hasMany(Consulta::class, 'expediente_id');
    }

    public function paciente()
    {
        return $this->belongsTo(Paciente::class, 'paciente_id');
    }

    public function antecedentesPatologicos()
    {
        return $this->hasMany(ExpedienteAntecedentePatologico::class, 'expediente_id');
    }

    public function antecedentesNoPatologicos()
    {
        return $this->hasMany(ExpedienteAntecedenteNoPatologico::class, 'expediente_id');
    }

    public function antecedentesFamiliares()
    {
        return $this->hasMany(ExpedienteAntecedenteFamiliar::class, 'expediente_id');
    }
}

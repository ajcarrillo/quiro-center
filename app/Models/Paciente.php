<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    protected $table    = 'pacientes';
    protected $fillable = [
        'user_id',
        'fecha_nacimiento',
        'sexo',
        'estado_civil',
        'escolaridad',
        'lugar_origen',
        'ocupacion',
        'contactos_emergencia',
        'servicio_medico',
        'tipo_sangre',
    ];
    protected $casts    = [
        'contactos_emergencia' => 'array',
    ];

    public function domicilio()
    {
        return $this->morphOne(Domicilio::class, 'addressable')
            ->withDefault([ 'empty' => true ]);
    }

    public function expediente()
    {
        return $this->hasOne(Expediente::class, 'paciente_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function setEscolaridadAttribute($value)
    {
        $this->attributes['escolaridad'] = mb_strtoupper($value, 'UTF-8');
    }
}

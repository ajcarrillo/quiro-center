<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/26/20
 * Time: 12:53 a. m.
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ExpedienteAntecedenteFamiliar extends Model
{
    protected $table    = 'expediente_antecedentes_familiares';
    protected $fillable = [
        'expediente_id',
        'antecedente_id',
        'observaciones',
    ];

    public function antecedente()
    {
        return $this->belongsTo(AntecedenteFamiliar::class, 'antecedente_id');
    }
}

<?php

namespace App;

use App\Models\Paciente;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'last_name',
        'full_name',
        'email',
        'phone',
        'password',
        'api_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function paciente()
    {
        return $this->hasOne(Paciente::class, 'user_id')
            ->withDefault([ 'full_name' => 'PACIENTE NO REGISTRADO' ]);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = mb_strtoupper($value, 'UTF-8');
    }

    public function setLastNameAttribute($value)
    {
        $this->attributes['last_name'] = mb_strtoupper($value, 'UTF-8');
    }

    public function setFullNameAttribute($value)
    {
        $this->attributes['full_name'] = mb_strtoupper($value, 'UTF-8');
    }
}

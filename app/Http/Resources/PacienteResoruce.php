<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class PacienteResoruce extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'folio'            => str_pad($this->id, 6, "0", STR_PAD_LEFT),
            'fecha_nacimiento' => $this->fecha_nacimiento,
            'edad'             => Carbon::parse($this->fecha_nacimiento)->age,
            'sexo'             => $this->sexo == 'M' ? 'Masculino' : 'Femenino',
            'estado_civil'     => $this->estado_civil,
            'escolaridad'      => $this->escolaridad,
            'domicilio'        => $this->domicilio,
            'lugar_origen'     => $this->lugar_origen,
            'ocupacion'        => $this->ocupacion,
            'user'             => $this->user,
            'expediente'       => $this->expediente,
        ];
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/21/20
 * Time: 12:38 p. m.
 */

namespace App\Http\Controllers;


use App\Models\AntecedenteFamiliar;
use App\Models\AntecedenteNoPato;
use App\Models\AntecedentePato;
use App\Models\Articulacion;
use App\Models\Escolaridad;
use App\Models\Musculo;
use App\Models\ServicioMedico;

class AppController extends Controller
{
    public function index()
    {
        $antecedentesPato   = $this->getAntecedentesPato();
        $antecedentesNoPato = $this->getAntecedenesNoPato();
        $catAntecedentesFam = $this->getAntecedentesFamiliares();
        $articulaciones     = $this->getArticulaciones();
        $musculos           = $this->getMusculos();
        $escolaridades      = $this->getEscolaridades();
        $serviciosMedicos   = $this->getServiciosMedicos();

        return view('home', compact(
            'antecedentesNoPato',
            'antecedentesPato',
            'catAntecedentesFam',
            'articulaciones',
            'musculos',
            'escolaridades',
            'serviciosMedicos'
        ));
    }

    private function getAntecedentesPato()
    {
        return AntecedentePato::query()
            ->orderBy('descripcion')
            ->get([ 'id', 'descripcion' ]);
    }

    private function getAntecedenesNoPato()
    {
        return AntecedenteNoPato::query()
            ->orderBy('descripcion')
            ->get([ 'id', 'descripcion' ]);
    }

    private function getAntecedentesFamiliares()
    {
        return AntecedenteFamiliar::query()
            ->orderBy('descripcion')
            ->get([ 'id', 'descripcion' ]);
    }

    /**
     * @param $antecedentes
     * @return mixed
     */
    private function getMap($antecedentes)
    {
        return $antecedentes->map(function ($el) {
            return [
                'antecedente'    => $el,
                'expediente_id'  => NULL,
                'antecedente_id' => $el->id,
                'observaciones'  => NULL,
            ];
        });
    }

    private function getArticulaciones()
    {
        return Articulacion::query()
            ->orderBy('descripcion')
            ->get([ 'id', 'descripcion' ]);
    }

    private function getMusculos()
    {
        return Musculo::query()
            ->orderBy('descripcion')
            ->get([ 'id', 'descripcion' ]);
    }

    private function getEscolaridades()
    {
        return Escolaridad::query()
            ->orderBy('descripcion')
            ->pluck('descripcion');
    }

    private function getServiciosMedicos()
    {
        return ServicioMedico::query()
            ->orderBy('descripcion')
            ->pluck('descripcion');
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/25/20
 * Time: 1:00 p. m.
 */

namespace App\Http\Controllers\API\v1;


use App\Http\Controllers\Controller;
use App\Models\Expediente;
use Illuminate\Http\Request;

class UpdateTipoInterrogatorioController extends Controller
{
    public function update(Request $request, Expediente $expediente)
    {
        $expediente->update([ 'tipo_interrogatorio' => $request->input('tipo_interrogatorio') ]);

        return ok();
    }
}

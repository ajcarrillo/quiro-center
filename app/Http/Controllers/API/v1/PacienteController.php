<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/21/20
 * Time: 7:39 p. m.
 */

namespace App\Http\Controllers\API\v1;


use App\Http\Controllers\Controller;
use App\Http\Resources\PacienteResoruce;
use App\Models\Domicilio;
use App\Models\Escolaridad;
use App\Models\Expediente;
use App\Models\Paciente;
use App\User;
use DB;
use Illuminate\Http\Request;
use Str;

class PacienteController extends Controller
{
    public function index()
    {
        $pacientes = Paciente::with('user', 'expediente', 'domicilio')
            ->get();

        return PacienteResoruce::collection($pacientes);
    }

    public function store(Request $request)
    {
        list($escolaridad, $exists) = $this->checkEscolaridadExists($request);

        $paciente = DB::transaction(function () use ($request, $escolaridad, $exists) {

            if ( ! $exists) {
                Escolaridad::create([ 'descripcion' => mb_strtoupper($escolaridad, 'UTF-8') ]);
            }

            $user = User::create([
                'name'      => $request->input('name'),
                'last_name' => $request->input('last_name'),
                'full_name' => "{$request->input('name')} {$request->input('last_name')}",
                'email'     => $request->input('email'),
                'phone'     => $request->input('phone'),
                'password'  => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password,
                'api_token' => Str::random(60),
            ]);

            $paciente   = new Paciente($request->input());
            $expediente = new Expediente();
            $domicilio  = new Domicilio($request->input('domicilio'));

            $user->paciente()->save($paciente);

            $expediente->paciente()->associate($paciente);

            $paciente->domicilio()->save($domicilio);

            $expediente->save();

            return $paciente;
        });

        $paciente->load('expediente');

        return ok(compact('paciente'));
    }

    public function update(Request $request, Paciente $paciente)
    {
        list($escolaridad, $exists) = $this->checkEscolaridadExists($request);

        DB::transaction(function () use ($request, $paciente, $escolaridad, $exists) {

            if ( ! $exists) {
                Escolaridad::create([ 'descripcion' => mb_strtoupper($escolaridad, 'UTF-8') ]);
            }

            $userData = $request->only([
                'name',
                'last_name',
                'email',
                'phone',
            ]);

            $pacienteData = $request->only([
                'fecha_nacimiento',
                'sexo',
                'estado_civil',
                'escolaridad',
                'domicilio',
                'lugar_origen',
                'ocupacion',
            ]);

            $domicilioData = $request->only([
                'domicilio.codigo_postal_id',
                'domicilio.entidad_id',
                'domicilio.municipio_id',
                'domicilio.localidad_id',
                'domicilio.colonia',
                'domicilio.calle',
                'domicilio.numero',
                'domicilio.cruzamiento_1',
                'domicilio.cruzamiento_2',
            ]);

            $paciente->update($pacienteData);

            $paciente->user()->update($userData);

            $paciente->domicilio()->update($domicilioData['domicilio']);
        });

        $paciente->load('user', 'domicilio');

        return ok(compact('paciente'));
    }

    /**
     * @param Request $request
     * @return array
     */
    private function checkEscolaridadExists(Request $request): array
    {
        $escolaridad = $request->input('escolaridad');

        $exists = Escolaridad::query()
            ->where('descripcion', $escolaridad)
            ->exists();

        return array( $escolaridad, $exists );
    }
}

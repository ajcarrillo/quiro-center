<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/27/20
 * Time: 1:04 a. m.
 */

namespace App\Http\Controllers\API\v1;


use App\Http\Controllers\Controller;
use App\Models\Consulta;
use App\Models\Expediente;

class ExpedienteUltimaConsutlaController extends Controller
{
    public function show(Expediente $expediente)
    {
        $consulta = Consulta::query()
            ->where('expediente_id', $expediente->id)
            ->orderBy('fecha', 'desc')
            ->firstOr([ '*' ], function () {
                return [
                    'talla'                   => 0,
                    'frecuencia_cardiaca'     => 0,
                    'frecuencia_respiratoria' => 0,
                    'temperatura'             => 0,
                    'peso'                    => 0,
                    'cintura'                 => 0,
                    'fecha'                   => NULL,
                ];
            });

        return ok(compact('consulta'));
    }
}

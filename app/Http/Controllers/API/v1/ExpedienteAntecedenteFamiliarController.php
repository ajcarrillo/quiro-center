<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/26/20
 * Time: 12:02 p. m.
 */

namespace App\Http\Controllers\API\v1;


use App\Http\Controllers\Controller;
use App\Models\Expediente;
use App\Models\ExpedienteAntecedenteFamiliar;
use Illuminate\Http\Request;

class ExpedienteAntecedenteFamiliarController extends Controller
{
    public function store(Request $request, Expediente $expediente)
    {
        ExpedienteAntecedenteFamiliar::updateOrCreate(
            [ 'expediente_id' => $expediente->id, 'antecedente_id' => $request->input('antecedente_id') ],
            [ 'observaciones' => $request->input('observaciones') ]
        );

        $expediente->load(
            'antecedentesFamiliares',
            'antecedentesFamiliares.antecedente'
        );

        return ok(compact('expediente'));
    }

    public function destroy(Expediente $expediente, $id)
    {
        $expediente->antecedentesFamiliares()
            ->where('id', $id)->delete();

        return ok();
    }
}

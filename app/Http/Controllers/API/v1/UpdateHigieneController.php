<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/26/20
 * Time: 6:17 p. m.
 */

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Models\Expediente;
use Illuminate\Http\Request;

class UpdateHigieneController extends Controller
{
    public function update(Request $request, Expediente $expediente)
    {
        $expediente->update([ 'higiene' => $request->input('higiene') ]);

        return ok();
    }
}

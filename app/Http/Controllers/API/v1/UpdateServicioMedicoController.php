<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/30/20
 * Time: 1:42 p. m.
 */

namespace App\Http\Controllers\API\v1;


use App\Http\Controllers\Controller;
use App\Models\Paciente;
use App\Models\ServicioMedico;
use DB;
use Illuminate\Http\Request;

class UpdateServicioMedicoController extends Controller
{
    public function update(Request $request, Paciente $paciente)
    {
        $servicios      = [];
        $servicioMedico = $request->input('servicio_medico');
        $exists         = $this->checkServicioExists($servicioMedico);

        DB::transaction(function () use ($servicioMedico, $paciente, $exists) {

            if ( ! $exists) {
                $this->createServicio($servicioMedico);
            }

            $paciente->update([
                'servicio_medico' => mb_strtoupper($servicioMedico, 'UTF-8'),
            ]);
        });

        if ( ! $exists) {
            $servicios = ServicioMedico::query()->orderBy('descripcion')->pluck('descripcion');
        }

        return ok(compact('servicios'));
    }

    /**
     * @param $servicioMedico
     * @return bool
     */
    private function checkServicioExists($servicioMedico): bool
    {
        return ServicioMedico::query()
            ->where('descripcion', $servicioMedico)
            ->exists();
    }

    /**
     * @param $servicioMedico
     */
    private function createServicio($servicioMedico): void
    {
        ServicioMedico::create([ 'descripcion' => mb_strtoupper($servicioMedico, 'UTF-8') ]);
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/23/20
 * Time: 6:24 p. m.
 */

namespace App\Http\Controllers\API\v1;


use App\Http\Controllers\Controller;
use App\Models\Expediente;
use App\Models\ExpedienteAntecedentePatologico;
use Illuminate\Http\Request;

class ExpedienteAntecedentePatologicoController extends Controller
{
    public function store(Request $request, Expediente $expediente)
    {
        ExpedienteAntecedentePatologico::updateOrCreate(
            [ 'expediente_id' => $expediente->id, 'antecedente_id' => $request->input('antecedente_id') ],
            [ 'observaciones' => $request->input('observaciones') ]
        );

        $expediente->load(
            'antecedentesPatologicos',
            'antecedentesPatologicos.antecedente'
        );

        return ok(compact('expediente'));
    }

    public function destroy(Expediente $expediente, $id)
    {
        $expediente->antecedentesPatologicos()
            ->where('id', $id)
            ->delete();

        return ok();
    }
}

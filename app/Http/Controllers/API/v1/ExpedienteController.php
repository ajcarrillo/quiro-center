<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/23/20
 * Time: 9:40 p. m.
 */

namespace App\Http\Controllers\API\v1;


use App\Http\Controllers\Controller;
use App\Models\Expediente;

class ExpedienteController extends Controller
{
    public function show(Expediente $expediente)
    {
        $expediente->load(
            'paciente',
            'paciente.user',
            'paciente.domicilio',
            'antecedentesPatologicos',
            'antecedentesPatologicos.antecedente',
            'antecedentesNoPatologicos',
            'antecedentesNoPatologicos.antecedente',
            'antecedentesFamiliares',
            'antecedentesFamiliares.antecedente'
        );

        return ok(compact('expediente'));
    }
}

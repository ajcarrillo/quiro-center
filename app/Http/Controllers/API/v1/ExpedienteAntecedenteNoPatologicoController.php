<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/23/20
 * Time: 6:24 p. m.
 */

namespace App\Http\Controllers\API\v1;


use App\Http\Controllers\Controller;
use App\Models\Expediente;
use App\Models\ExpedienteAntecedenteNoPatologico;
use Illuminate\Http\Request;

class ExpedienteAntecedenteNoPatologicoController extends Controller
{
    public function store(Request $request, Expediente $expediente)
    {
        ExpedienteAntecedenteNoPatologico::updateOrCreate(
            [ 'expediente_id' => $expediente->id, 'antecedente_id' => $request->input('antecedente_id') ],
            [ 'observaciones' => $request->input('observaciones') ]
        );

        $expediente->load(
            'antecedentesNoPatologicos',
            'antecedentesNoPatologicos.antecedente'
        );

        return ok(compact('expediente'));
    }

    public function destroy(Expediente $expediente, $id)
    {
        $expediente->antecedentesNoPatologicos()
            ->where('id', $id)->delete();

        return ok();
    }
}

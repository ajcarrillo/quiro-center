<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/27/20
 * Time: 1:40 a. m.
 */

namespace App\Http\Controllers\API\v1;


use App\Http\Controllers\Controller;
use App\Models\Consulta;
use App\Models\Expediente;
use Carbon\Carbon;

class ConsultaController extends Controller
{
    public function store(Expediente $expediente)
    {
        $consulta = new Consulta([
            'expediente_id' => $expediente->id,
            'fecha'         => Carbon::now(),
        ]);

        $consulta->save();

        return ok(compact('consulta'));
    }
}

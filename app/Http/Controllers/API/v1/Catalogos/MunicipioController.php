<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/29/20
 * Time: 6:56 p. m.
 */

namespace App\Http\Controllers\API\v1\Catalogos;


use App\Http\Controllers\Controller;
use App\Models\Municipio;
use Illuminate\Http\Request;

class MunicipioController extends Controller
{
    public function index(Request $request)
    {
        $entidadId = $request->query('entidad_id');

        $municipios = Municipio::query()
            ->where('entidad_id', $entidadId)
            ->orderBy('descripcion')
            ->get();

        return ok(compact('municipios'));
    }
}

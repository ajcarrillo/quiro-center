<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/29/20
 * Time: 5:00 p. m.
 */

namespace App\Http\Controllers\API\v1\Catalogos;


use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class CodigoPostalController extends Controller
{
    public function show(Request $request)
    {
        $colonias = DB::table('codigos_postales as cp')
            ->select([
                'd_codigo',
                'ge.entidad_id',
                'ge.descripcion as entidad_d',
                'gm.municipio_id',
                'gm.descripcion as municipio_d',
                'gl.localidad_id',
                'gl.descripcion as localidad_d',
                'colonia',
            ])
            ->join('geo_entidades as ge', 'cp.entidad_id', '=', 'ge.entidad_id')
            ->join('geo_municipios as gm', function ($join) {
                $join->on('cp.municipio_id', '=', 'gm.municipio_id')
                    ->on('cp.entidad_id', '=', 'gm.entidad_id');
            })
            ->leftJoin('geo_localidades as gl', function ($join) {
                $join->on('cp.entidad_id', '=', 'gl.entidad_id')
                    ->on('cp.municipio_id', '=', 'gl.municipio_id')
                    ->on('cp.d_ciudad', '=', 'gl.descripcion');
            })
            ->where('cp.d_codigo', $request->query('codigo_postal'))
            ->get();

        $geo = [
            'entidad_id'   => $colonias[0]->entidad_id,
            'municipio_id' => $colonias[0]->municipio_id,
            'localidad_id' => $colonias[0]->localidad_id,
        ];

        return compact('colonias', 'geo');
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/29/20
 * Time: 6:59 p. m.
 */

namespace App\Http\Controllers\API\v1\Catalogos;


use App\Http\Controllers\Controller;
use App\Models\Localidad;
use Illuminate\Http\Request;

class LocalidadController extends Controller
{
    public function index(Request $request)
    {
        $entidadId   = $request->query('entidad_id');
        $municipioId = $request->query('municipio_id');

        $localidades = Localidad::query()
            ->where('entidad_id', $entidadId)
            ->where('municipio_id', $municipioId)
            ->orderBy('descripcion')
            ->get();

        return ok(compact('localidades'));
    }
}

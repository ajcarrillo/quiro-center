<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/30/20
 * Time: 5:43 p. m.
 */

namespace App\Http\Controllers\API\v1;


use App\Http\Controllers\Controller;
use App\Models\Paciente;
use Illuminate\Http\Request;

class UpdateContactosEmergenciaController extends Controller
{
    public function update(Request $request, Paciente $paciente)
    {
        $contactos = $request->input('contactos');

        $paciente->update([ 'contactos_emergencia' => $contactos ]);

        return ok();
    }

}

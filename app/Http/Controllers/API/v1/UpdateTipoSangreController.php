<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/30/20
 * Time: 11:54 a. m.
 */

namespace App\Http\Controllers\API\v1;


use App\Http\Controllers\Controller;
use App\Models\Paciente;
use Illuminate\Http\Request;

class UpdateTipoSangreController extends Controller
{
    public function update(Request $request, Paciente $paciente)
    {
        $paciente->update([
            'tipo_sangre' => $request->input('tipo_sangre'),
        ]);

        return ok();
    }
}

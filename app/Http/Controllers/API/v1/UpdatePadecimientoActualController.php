<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/26/20
 * Time: 4:33 p. m.
 */

namespace App\Http\Controllers\API\v1;


use App\Http\Controllers\Controller;
use App\Models\Expediente;
use Illuminate\Http\Request;

class UpdatePadecimientoActualController extends Controller
{
    public function update(Request $request, Expediente $expediente)
    {
        $expediente->update($request->only([
            'padecimiento_actual',
            'dx_envio',
            'medico_tratante',
        ]));

        return ok();
    }
}

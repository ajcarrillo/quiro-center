CREATE TABLE `codigos_postales` (
    `d_codigo`         CHAR(5) COLLATE utf8mb4_unicode_ci      DEFAULT NULL,
    `c_estado`         CHAR(2) COLLATE utf8mb4_unicode_ci      DEFAULT NULL,
    `c_mnpio`          CHAR(3) COLLATE utf8mb4_unicode_ci      DEFAULT NULL,
    `c_cve_ciudad`     TEXT COLLATE utf8mb4_unicode_ci         DEFAULT NULL,
    `colonia`          VARCHAR(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `entidad_id`       INT(11)                                 DEFAULT NULL,
    `municipio_id`     INT(11)                                 DEFAULT NULL,
    `d_tipo_asenta`    TEXT COLLATE utf8mb4_unicode_ci         DEFAULT NULL,
    `d_mnpio`          TEXT COLLATE utf8mb4_unicode_ci         DEFAULT NULL,
    `d_estado`         TEXT COLLATE utf8mb4_unicode_ci         DEFAULT NULL,
    `d_ciudad`         VARCHAR(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `d_cp`             CHAR(5) COLLATE utf8mb4_unicode_ci      DEFAULT NULL,
    `c_oficina`        TEXT COLLATE utf8mb4_unicode_ci         DEFAULT NULL,
    `c_cp`             TEXT COLLATE utf8mb4_unicode_ci         DEFAULT NULL,
    `c_tipo_asenta`    TEXT COLLATE utf8mb4_unicode_ci         DEFAULT NULL,
    `id_asenta_cpcons` TEXT COLLATE utf8mb4_unicode_ci         DEFAULT NULL,
    `d_zona`           TEXT COLLATE utf8mb4_unicode_ci         DEFAULT NULL,
    KEY `codigos_postales_d_codigo_index` (`d_codigo`),
    KEY `codigos_postales_c_estado_index` (`c_estado`),
    KEY `codigos_postales_c_mnpio_index` (`c_mnpio`),
    KEY `codigos_postales_colonia_index` (`colonia`),
    KEY `codigos_postales_entidad_id_index` (`entidad_id`),
    KEY `codigos_postales_municipio_id_index` (`municipio_id`),
    KEY `codigos_postales_d_ciudad_index` (`d_ciudad`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8mb4
    COLLATE = utf8mb4_unicode_ci


<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveFieldsFromExpedientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expedientes', function (Blueprint $table) {
            $table->dropColumn('habitaciones');
            $table->dropColumn('tipo_interrogatorio');
            $table->dropColumn('padecimiento_actual');
            $table->dropColumn('dx_envio');
            $table->dropColumn('medico_tratante');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expedientes', function (Blueprint $table) {
            $table->string('habitaciones')->nullable();
            $table->string('tipo_interrogatorio')->nullable();
            $table->string('padecimiento_actual')->nullable();
            $table->string('dx_envio')->nullable();
            $table->string('medico_tratante')->nullable();
        });
    }
}

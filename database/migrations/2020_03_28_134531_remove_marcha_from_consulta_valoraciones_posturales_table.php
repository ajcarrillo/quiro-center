<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveMarchaFromConsultaValoracionesPosturalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('consulta_valoraciones_posturales', function (Blueprint $table) {
            $table->dropColumn('marcha');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consulta_valoraciones_posturales', function (Blueprint $table) {
            $table->text('marcha')
                ->nullable()
                ->after('derecha');
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeoLocalidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo_localidades', function (Blueprint $table) {
            $table->bigInteger('localidad_id');
            $table->unsignedTinyInteger('entidad_id');
            $table->foreign('entidad_id')->references('entidad_id')->on('geo_entidades');
            $table->bigInteger('municipio_id');
            $table->foreign('municipio_id')->references('municipio_id')->on('geo_municipios');
            $table->char('cve_ent', 2);
            $table->char('cve_mun', 3);
            $table->char('cve_loc', 4);
            $table->string('descripcion');
            $table->primary([ 'entidad_id', 'municipio_id', 'localidad_id' ]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_localidades');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeoMunicipiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo_municipios', function (Blueprint $table) {
            $table->bigInteger('municipio_id');
            $table->unsignedTinyInteger('entidad_id');
            $table->foreign('entidad_id')->references('entidad_id')->on('geo_entidades');
            $table->char('cve_ent', 2);
            $table->char('cve_mun', 3);
            $table->string('descripcion');
            $table->primary([ 'municipio_id', 'entidad_id' ]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_municipios');
    }
}

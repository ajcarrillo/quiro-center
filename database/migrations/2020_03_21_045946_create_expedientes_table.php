<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpedientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expedientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('paciente_id');
            $table->foreign('paciente_id')->references('id')->on('pacientes');
            $table->string('tipo_interrogatorio')->nullable();
            $table->char('higiene', 1)->comment('B: Buena, R: Regular, M:Mala')->nullable();
            $table->char('alimentacion', 1)->comment('B: Buena, R: Regular, M:Mala')->nullable();
            $table->string('habitaciones')->nullable();
            $table->text('observaciones')->nullable();
            $table->text('padecimiento_actual')->nullable();
            $table->text('dx_envio')->nullable();
            $table->text('medico_tratante')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expedientes');
    }
}

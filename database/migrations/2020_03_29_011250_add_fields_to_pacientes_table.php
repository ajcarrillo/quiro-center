<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToPacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pacientes', function (Blueprint $table) {
            $table->char('tipo_sangre', 3)
                ->after('ocupacion')
                ->nullable();
            $table->string('servicio_medico')
                ->after('ocupacion')
                ->nullable();
            $table->text('contactos_emergencia')
                ->after('ocupacion')
                ->nullable();

            $table->dropColumn('domicilio');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pacientes', function (Blueprint $table) {
            $table->dropColumn('tipo_sangre');
            $table->dropColumn('servicio_medico');
            $table->dropColumn('contactos_emergencia');
            $table->string('domicilio')->nullable();
        });
    }
}

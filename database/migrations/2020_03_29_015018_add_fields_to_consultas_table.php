<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToConsultasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('consultas', function (Blueprint $table) {
            $table->string('padecimiento_actual')
                ->after('fecha')
                ->nullable();
            $table->string('dx_envio')
                ->after('fecha')
                ->nullable();
            $table->string('medico_tratante')
                ->after('fecha')
                ->nullable();
            $table->unsignedBigInteger('consulta_padre_id')->after('id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consultas', function (Blueprint $table) {
            $table->dropColumn('padecimiento_actual');
            $table->dropColumn('dx_envio');
            $table->dropColumn('medico_tratante');
            $table->dropColumn('consulta_padre_id');
        });
    }
}

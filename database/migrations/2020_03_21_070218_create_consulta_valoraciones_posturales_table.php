<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConsultaValoracionesPosturalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consulta_valoraciones_posturales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('consulta_id');
            $table->foreign('consulta_id')->references('id')->on('consultas');
            $table->unsignedBigInteger('articulacion_id');
            $table->foreign('articulacion_id')->references('id')->on('articulaciones');
            $table->text('vista_anterior')->nullable();
            $table->text('vista_lateral')->nullable();
            $table->text('vista_posterior')->nullable();
            $table->string('izquierda')->nullable();
            $table->string('derecha')->nullable();
            $table->text('marcha')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consulta_valoraciones_posturales');
    }
}

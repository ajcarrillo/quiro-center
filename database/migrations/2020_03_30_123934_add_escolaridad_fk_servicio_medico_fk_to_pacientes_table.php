<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEscolaridadFkServicioMedicoFkToPacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pacientes', function (Blueprint $table) {
            $table->foreign('escolaridad')
                ->references('descripcion')
                ->on('escolaridades');
            $table->foreign('servicio_medico')
                ->references('descripcion')
                ->on('servicios_medicos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pacientes', function (Blueprint $table) {
            $table->dropForeign([ 'escolaridad' ]);
            $table->dropForeign([ 'servicio_medico' ]);
        });
    }
}

<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */

    const TABLES = [
        'antecedentes_no_patologicos',
        'antecedentes_patologicos',
        'articulaciones',
        'musculos',
    ];

    public function run()
    {
        $this->truncateTables();

        $this->call(UsersTableSeeder::class);
        $this->call(CatalogosTableSeeder::class);
    }

    private function truncateTables()
    {
        $this->checkForeignKeys(false);
        foreach (self::TABLES as $TABLE) {
            DB::table($TABLE)->truncate();
        }
        $this->checkForeignKeys(true);
    }

    private function checkForeignKeys($check)
    {
        $check = $check ? '1' : '0';
        DB::statement('SET FOREIGN_KEY_CHECKS = ' . $check);
    }
}

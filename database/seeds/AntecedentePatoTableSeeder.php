<?php

use App\Models\AntecedentePato;
use Illuminate\Database\Seeder;

class AntecedentePatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            'ENFERMEDADES CONGÉNITAS',
            'ENFERMEDADES PROPIAS DE LA INFANCIA',
            'TERAPÉUTICA EMPLEADA',
            'QUIRÚRGICOS',
            'TRAUMÁTICOS',
            'ALÉRGICOS',
            'TRANSFUNCIONALES',
            'INTOXICACIONES',
            'DE HOSPITALIZACIÓN',
            'OTRAS ENFERMEDADES',
        ];

        foreach ($items as $item) {
            AntecedentePato::create([ 'descripcion' => $item ]);
        }
    }
}

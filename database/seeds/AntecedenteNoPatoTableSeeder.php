<?php

use App\Models\AntecedenteNoPato;
use Illuminate\Database\Seeder;

class AntecedenteNoPatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            'TABAQUISMO',
            'ALCOHOLISMO',
            'TOXICOMANÍAS',
            'EJERCICIO',
            'ZOONOSIS',
            'INMUNIZACIONES',
            'PRÓTESIS',
            'PASATIEMPOS',
        ];

        foreach ($items as $item) {
            AntecedenteNoPato::create([ 'descripcion' => $item ]);
        }
    }
}

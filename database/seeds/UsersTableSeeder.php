<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class, 1)->create([
            'name'      => 'AIME',
            'last_name' => 'RODRIGUEZ',
            'full_name' => 'AIME RODRIGUEZ',
            'email'     => 'aime@sample.com',
            'phone'     => '9831307514',
        ]);
    }
}

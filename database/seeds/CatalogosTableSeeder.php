<?php

use Illuminate\Database\Seeder;

class CatalogosTableSeeder extends Seeder
{
    const TABLES = [
        'antecedentes_no_patologicos',
        'antecedentes_patologicos',
        'antecedentes_heredo_familiares',
        'articulaciones',
        'musculos',
        'servicios_medicos',
        'escolaridades',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTables();

        $musculos               = $this->getMusculos();
        $articulaciones         = $this->getArticulaciones();
        $antecedentesFamiliares = $this->getAntecedentesFamiliares();
        $escolaridades          = $this->getEscolaridades();
        $serviciosMedicos       = $this->getServiciosMedicos();

        foreach ($antecedentesFamiliares as $item) {
            \App\Models\AntecedenteFamiliar::create([ 'descripcion' => $item ]);
        }

        foreach ($articulaciones as $articulacion) {
            \App\Models\Articulacion::create([ 'descripcion' => $articulacion ]);
        }

        foreach ($musculos as $musculo) {
            \App\Models\Musculo::create([ 'descripcion' => $musculo ]);
        }

        foreach ($escolaridades as $escolaridad) {
            \App\Models\Escolaridad::create([ 'descripcion' => $escolaridad ]);
        }

        foreach ($serviciosMedicos as $servicio) {
            \App\Models\ServicioMedico::create([ 'descripcion' => $servicio ]);
        }

        $this->call(AntecedenteNoPatoTableSeeder::class);
        $this->call(AntecedentePatoTableSeeder::class);
    }

    /**
     * @return array
     */
    private function getMusculos(): array
    {
        $musculos = [
            'Ecom',
            'Escalenos',
            'Deltoides',
            'Supraespinoso',
            'Infraespinoso',
            'Redondos',
            'Subescapular',
            'Bicept',
            'Tricept',
            'Epicondilios',
            'Epitrocleares',
            'Bicept Femoral',
            'Vasto Interno',
            'Vasto Externo',
            'Recto Femoral',
            'Vasto medio',
            'Semimembranoso',
            'Semitendinoso',
            'Peroneos',
            'Tibiales',
            'Pectoral mayor',
            'Pectoral Menor',
            'Popitleo',
            'Recto Abdominal',
            'Oblicuos',
            'Soleo',
            'Gemelos',
            'Gluteo mayor',
            'Gluteo Menor',
            'Gluteo Medio',
            'Tensor de la Fascia lata',
            'Abductores',
            'Aductores',
        ];

        return $musculos;
    }

    /**
     * @return array
     */
    private function getArticulaciones(): array
    {
        $articulaciones = [
            'Artic. Glenohumeral',
            'Artic. Subdeltoidea',
            'Artic. Cubitotroclear',
            'Artic. Radiocubital',
            'Artic. Radiocarpiana',
            'Artic. Coxofemoral',
            'Artic. Femorotibiales',
            'Artic. Subastragalina',
        ];

        return $articulaciones;
    }

    public function getAntecedentesFamiliares(): array
    {
        return [
            'CARDIOVASCULARES',
            'PULMONARES',
            'GASTROINTESTINALES',
            'URINARIOS',
            'HEMATOLÓGICAS',
            'ENDÓCRINAS',
            'DE LOS SENTIDOS',
            'ALERGIAS',
            'GINECOLÓGICAS',
            'CIRUGÍAS',
            'OSTEOARTICULARES',
            'NEUROLÓGICAS',
            'MENTALES',
            'INFECCIOSAS',
            'METABÓLICAS',
            'MALFORMACIONES',
            'NEOPLASIAS',
            'TOXICOMANÍAS',
        ];
    }

    private function truncateTables()
    {
        $this->checkForeignKeys(false);
        foreach (self::TABLES as $TABLE) {
            DB::table($TABLE)->truncate();
        }
        $this->checkForeignKeys(true);
    }

    private function checkForeignKeys($check)
    {
        $check = $check ? '1' : '0';
        DB::statement('SET FOREIGN_KEY_CHECKS = ' . $check);
    }

    private function getEscolaridades()
    {
        return [
            'SIN ESTUDIOS',
            'PRIMARIA',
            'SECUNDARIA',
            'BACHILLERATO',
            'LICENCIATURA',
            'INGENIERIA',
            'MAESTRIA',
            'POSGRADO',
        ];
    }

    private function getServiciosMedicos()
    {
        return [
            'NINGUNO',
            'ISSSTE',
            'IMSS',
            'MARINA',
            'EJERCITO',
            'SEGURO POPULAR',
            'INSABI',
        ];
    }
}

<?php

use App\Http\Controllers\API\v1\Catalogos\CodigoPostalController;
use App\Http\Controllers\API\v1\Catalogos\LocalidadController;
use App\Http\Controllers\API\v1\Catalogos\MunicipioController;
use App\Http\Controllers\API\v1\ConsultaController;
use App\Http\Controllers\API\v1\ExpedienteController;
use App\Http\Controllers\API\v1\ExpedienteUltimaConsutlaController;
use App\Http\Controllers\API\v1\UpdateAlimentacionController;
use App\Http\Controllers\API\v1\UpdateContactosEmergenciaController;
use App\Http\Controllers\API\v1\UpdateHabitacionController;
use App\Http\Controllers\API\v1\UpdateHigieneController;
use App\Http\Controllers\API\v1\UpdatePadecimientoActualController;
use App\Http\Controllers\API\v1\UpdateServicioMedicoController;
use App\Http\Controllers\API\v1\UpdateTipoInterrogatorioController;
use App\Http\Controllers\API\v1\UpdateTipoSangreController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')
    ->name('api.v1.')
    ->group(function () {
        Route::prefix('catalogos')
            ->name('catalogos.')
            ->group(function () {
                Route::get('codigos-postales', [ CodigoPostalController::class, 'show' ])
                    ->name('codigos.postales');
                Route::get('localidades', [ LocalidadController::class, 'index' ])
                    ->name('localidades');
                Route::get('municipios', [ MunicipioController::class, 'index' ])
                    ->name('municipios');
            });

        Route::prefix('consultas')
            ->name('consultas.')
            ->group(function () {
                Route::post('/', [ ConsultaController::class, 'store' ])
                    ->name('store');
            });

        Route::prefix('expedientes')
            ->name('expedientes.')
            ->group(function () {
                Route::prefix('/{expediente}')
                    ->group(function () {
                        Route::get('/', [ ExpedienteController::class, 'show' ])
                            ->name('show');

                        Route::patch('/alimentacion', [ UpdateAlimentacionController::class, 'update' ])
                            ->name('alimentacion.update');

                        Route::apiResource('/antecedentes-familiares', 'API\v1\ExpedienteAntecedenteFamiliarController')
                            ->parameter('antecedentes-familiares', 'id')
                            ->only([ 'store', 'destroy' ])
                            ->names([
                                'store'   => 'antecedentes.familiares.store',
                                'destroy' => 'antecedentes.familiares.destroy',
                            ]);

                        Route::apiResource('/antecedentes-no-patologicos', 'API\v1\ExpedienteAntecedenteNoPatologicoController')
                            ->parameter('antecedentes-no-patologicos', 'id')
                            ->only([ 'store', 'destroy' ])
                            ->names([
                                'store'   => 'antecedentes.no.patologicos.store',
                                'destroy' => 'antecedentes.no.patologicos.destroy',
                            ]);

                        Route::apiResource('/antecedentes-patologicos', 'API\v1\ExpedienteAntecedentePatologicoController')
                            ->parameter('antecedentes-patologicos', 'id')
                            ->only([ 'store', 'destroy' ])
                            ->names([
                                'store'   => 'antecedentes.patologicos.store',
                                'destroy' => 'antecedentes.patologicos.destroy',
                            ]);

                        Route::patch('/habitaciones', [ UpdateHabitacionController::class, 'update' ])
                            ->name('habitacion.update');

                        Route::patch('/higiene', [ UpdateHigieneController::class, 'update' ])
                            ->name('higiene.update');

                        Route::patch('/padeciemientos-actuales', [ UpdatePadecimientoActualController::class, 'update' ])
                            ->name('padecimientos.actuales.update');

                        Route::patch('/tipo-interrogatorio', [ UpdateTipoInterrogatorioController::class, 'update' ])
                            ->name('tipo.interrogatorio.update');

                        Route::get('/ultima-consulta', [ ExpedienteUltimaConsutlaController::class, 'show' ])
                            ->name('ultima.consulta.show');
                    });
            });

        Route::apiResource('pacientes', 'API\v1\PacienteController')
            ->parameter('pacientes', 'paciente')
            ->only([ 'index', 'store', 'update' ])
            ->names([
                'index'  => 'pacientes.index',
                'store'  => 'pacientes.store',
                'update' => 'pacientes.update',
            ]);

        Route::prefix('pacientes')
            ->name('pacientes.')
            ->group(function () {
                Route::prefix('{paciente}')
                    ->group(function () {
                        Route::patch('contactos-emergencia', [ UpdateContactosEmergenciaController::class, 'update' ])
                            ->name('contactos.emergencia');
                        Route::patch('servicio-medico', [ UpdateServicioMedicoController::class, 'update' ])
                            ->name('servicio.medico');
                        Route::patch('tipo-sangre', [ UpdateTipoSangreController::class, 'update' ])
                            ->name('tipo.sangre');
                    });
            });
    });

import '@mdi/font/css/materialdesignicons.min.css'
import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import es from 'vuetify/es5/locale/es'
import colors from 'vuetify/lib/util/colors'


Vue.use(Vuetify)

const opts = {
    lang: {
        locales: {es},
        current: 'es',
    },
    theme: {
        themes: {
            light: {
                primary: colors.indigo.darken3, // #E53935
                secondary: colors.indigo.lighten4, // #FFCDD2
                accent: colors.pink.accent2, // #3F51B5
                error: '#FF5252',
                info: '#2196F3',
                success: colors.pink.accent2,
                warning: '#FFC107',
            },
        },
    },
}

export default new Vuetify(opts)

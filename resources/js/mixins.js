export const uniqueArrayJsonObjects = {
    methods: {
        mergeUniqueArray(arr, comp) {
            return arr.map(item => item[comp])
                // store the keys of the unique objects
                .map((item, i, final) => final.indexOf(item) === i && i)
                // eliminate the duplicate keys & store unique objects
                .filter(item => arr[item]).map(item => arr[item])
        },
    }
}

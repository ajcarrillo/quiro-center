require('./bootstrap');

window.Vue = require('vue');

import vuetify from './vuetify'
import router from './router'
import store from './store'
import VeeValidate from 'vee-validate'
import VeeValidateEs from './utilities/vee-validate-es'

import './filters/filters'

Vue.use(VeeValidate, {
    locale: 'es',
    dictionary: {
        es: {messages: VeeValidateEs}
    }
})

const app = new Vue({
    vuetify,
    router,
    store,
    created() {
        store.commit('catalogos/SET_ANTECEDENTES_PATO', window.antecedentesPato)
        store.commit('catalogos/SET_ANTECEDENTES_NO_PATO', window.antecedentesNoPato)
        store.commit('catalogos/SET_ANTECEDENTES_FAM', window.catAntecedentesFam)
        store.commit('catalogos/SET_ARTICULACIONES', window.articulaciones)
        store.commit('catalogos/SET_MUSCULOS', window.musculos)
        store.commit('catalogos/SET_ESCOLARIDADES', window.escolaridades)
        store.commit('catalogos/SET_SERVICIOS_MEDICOS', window.serviciosMedicos)
    },
    data: () => ({
        loading: false,
        hidePassword: true,
    }),
    methods: {
        logout() {
            document.getElementById('logout-form').submit();
        }
    }
}).$mount('#app');

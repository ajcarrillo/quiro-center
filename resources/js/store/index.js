import Vue from 'vue'
import Vuex from 'vuex'

import Catalogos from './catalogos/store'
import Expediente from './profile/store'

Vue.use(Vuex)

const vx = new Vuex.Store({
    modules: {
        catalogos: Catalogos,
        expediente: Expediente
    }
})

export default vx;

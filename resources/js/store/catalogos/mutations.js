export default {
    SET_ANTECEDENTES_PATO(state, payload) {
        state.antecedentesPato = payload
    },
    SET_ANTECEDENTES_NO_PATO(state, payload) {
        state.antecedentesNoPato = payload
    },
    SET_ANTECEDENTES_FAM(state, payload) {
        state.catAntecedentesFam = payload
    },
    SET_ARTICULACIONES(state, payload) {
        state.articulaciones = payload
    },
    SET_MUSCULOS(state, payload) {
        state.musculos = payload
    },
    SET_ESCOLARIDADES(state, payload) {
        state.escolaridades = payload
    },
    SET_SERVICIOS_MEDICOS(state, payload) {
        state.serviciosMedicos = payload
    },
}

export default {
    getAntecedenteById: state => id => {
        return state.antecedentesPato.find(function (el) {
            return el.id === id
        })
    }
}

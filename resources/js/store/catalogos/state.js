export default {
    antecedentesPato: [],
    antecedentesNoPato: [],
    catAntecedentesFam: [],
    articulaciones: [],
    musculos: [],
    tiposSangre: [
        'A+',
        'A-',
        'B+',
        'B-',
        'AB+',
        'AB-',
        'O+',
        'O-',
    ],
    entidades: [
        {
            'entidad_id': 1,
            'descripcion': 'AGUASCALIENTES'
        },
        {
            'entidad_id': 2,
            'descripcion': 'BAJA CALIFORNIA'
        },
        {
            'entidad_id': 3,
            'descripcion': 'BAJA CALIFORNIA SUR'
        },
        {
            'entidad_id': 4,
            'descripcion': 'CAMPECHE'
        },
        {
            'entidad_id': 5,
            'descripcion': 'COAHUILA DE ZARAGOZA'
        },
        {
            'entidad_id': 6,
            'descripcion': 'COLIMA'
        },
        {
            'entidad_id': 7,
            'descripcion': 'CHIAPAS'
        },
        {
            'entidad_id': 8,
            'descripcion': 'CHIHUAHUA'
        },
        {
            'entidad_id': 9,
            'descripcion': 'CIUDAD DE MÉXICO'
        },
        {
            'entidad_id': 10,
            'descripcion': 'DURANGO'
        },
        {
            'entidad_id': 11,
            'descripcion': 'GUANAJUATO'
        },
        {
            'entidad_id': 12,
            'descripcion': 'GUERRERO'
        },
        {
            'entidad_id': 13,
            'descripcion': 'HIDALGO'
        },
        {
            'entidad_id': 14,
            'descripcion': 'JALISCO'
        },
        {
            'entidad_id': 15,
            'descripcion': 'MÉXICO'
        },
        {
            'entidad_id': 16,
            'descripcion': 'MICHOACÁN DE OCAMPO'
        },
        {
            'entidad_id': 17,
            'descripcion': 'MORELOS'
        },
        {
            'entidad_id': 18,
            'descripcion': 'NAYARIT'
        },
        {
            'entidad_id': 19,
            'descripcion': 'NUEVO LEÓN'
        },
        {
            'entidad_id': 20,
            'descripcion': 'OAXACA'
        },
        {
            'entidad_id': 21,
            'descripcion': 'PUEBLA'
        },
        {
            'entidad_id': 22,
            'descripcion': 'QUERÉTARO'
        },
        {
            'entidad_id': 23,
            'descripcion': 'QUINTANA ROO'
        },
        {
            'entidad_id': 24,
            'descripcion': 'SAN LUIS POTOSÍ'
        },
        {
            'entidad_id': 25,
            'descripcion': 'SINALOA'
        },
        {
            'entidad_id': 26,
            'descripcion': 'SONORA'
        },
        {
            'entidad_id': 27,
            'descripcion': 'TABASCO'
        },
        {
            'entidad_id': 28,
            'descripcion': 'TAMAULIPAS'
        },
        {
            'entidad_id': 29,
            'descripcion': 'TLAXCALA'
        },
        {
            'entidad_id': 30,
            'descripcion': 'VERACRUZ DE IGNACIO DE LA LLAVE'
        },
        {
            'entidad_id': 31,
            'descripcion': 'YUCATÁN'
        },
        {
            'entidad_id': 32,
            'descripcion': 'ZACATECAS'
        },
        {
            'entidad_id': 36,
            'descripcion': 'ESTADOS UNIDOS MEXICANOS'
        },
        {
            'entidad_id': 97,
            'descripcion': 'NO APLICA'
        },
        {
            'entidad_id': 98,
            'descripcion': 'SE IGNORA'
        },
        {
            'entidad_id': 99,
            'descripcion': 'NO ESPECIFICADO'
        }
    ],
    escolaridades: [],
    serviciosMedicos: [],
}

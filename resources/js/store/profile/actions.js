import ExpedienteService from '../../services/ExpedienteService'
import PacienteService from '../../services/PacienteService'

export default {
    async fetchExpediente({commit}, expedienteId) {
        let result = await ExpedienteService.show(expedienteId)
        commit('SET_EXPEDIENTE', result.data.expediente)
    },
    async antecedentesPatoStore({commit}, payload) {
        let result = await ExpedienteService.antecedentesPatoStore(payload)
        commit('PUSH_ANTECEDENTE_PATO', result.data.expediente)
    },
    async antecedentesNoPatoStore({commit}, payload) {
        let result = await ExpedienteService.antecedentesNoPatoStore(payload)
        commit('PUSH_ANTECEDENTE_NO_PATO', result.data.expediente)
    },
    async antecedentesFamiliaresStore({commit}, payload) {
        let result = await ExpedienteService.antecedentesFamiliaresStore(payload)

        commit('PUSH_ANTECEDENTES_FAMILIARES', result.data.expediente)
    },
    async updatePaciente({commit}, payload) {
        let result = await ExpedienteService.updatePaciente(payload)

        commit('UPDATE_PACIENTE', result.data.paciente)
    },
    async destroyAntecedentePato({commit}, payload) {
        let result = await ExpedienteService.destroyAntecedentePato(payload)
        commit('REMOVE_ANTECEDENTE_PATO', payload)
    },
    async destroyAntecedenteNoPato({commit}, payload) {
        let result = await ExpedienteService.destroyAntecedenteNoPato(payload)
        commit('REMOVE_ANTECEDENTE_NO_PATO', payload)
    },
    async destroyAntecedenteFam({commit}, payload) {
        let result = await ExpedienteService.destroyAntecedenteFam(payload)
        commit('REMOVE_ANTECEDENTE_FAM', payload)
    },
    async updateTipoSangre({commit}, paylaod) {
        let result = await PacienteService.updateTipoSangre(paylaod)
        commit('UPDATE_TIPO_SANGRE', paylaod)
    },
    async updateServicioMedico({commit}, payload) {
        let result = await PacienteService.updateServicioMedico(payload)
        commit('UPDATE_SERVICIO_MEDICO', payload)
        return result
    },
    async updateContactosEmergencia({commit}, payload) {
        let result = await PacienteService.updateContactosEmergencia(payload)
        commit('UPDATE_CONTACTOS_EMERGENCIA', payload)
    }
}

export default {
    SET_EXPEDIENTE(state, payload) {
        state.expediente = payload
        state.antecedenes_pato = payload.antecedentes_patologicos
    },
    PUSH_ANTECEDENTE_PATO(state, payload) {
        Vue.set(state.expediente, 'antecedentes_patologicos', payload.antecedentes_patologicos)
    },
    RESET_ANTECEDENTE_PATO(state) {
        state.antecedenes_pato = []
    },
    ANTECEDENTE_PATO_DESTROY(state, payload) {
        let index = state.antecedenes_pato.findIndex(function (i) {
            return i.antecedente_id === payload.antecedentes_patologico;
        })

        Vue.delete(state.antecedenes_pato, index)
    },
    FETCHING_EXPEDIENTE(state, payload) {
        state.fetchingExpediente = payload
    },
    SAVING_ANTECEDENTE_PATO(state, payload) {
        state.savingAntecedentesPato = payload
    },
    PUSH_ANTECEDENTE_NO_PATO(state, payload) {
        Vue.set(state.expediente, 'antecedentes_no_patologicos', payload.antecedentes_no_patologicos)
    },
    SAVING_ANTECEDENTE_NO_PATO(state, payload) {
        state.savingAntecedentesNoPato = payload
    },
    UPDATE_TIPO_INTERROGATORIO(state, payload) {
        state.expediente.tipo_interrogatorio = payload
    },
    PUSH_ANTECEDENTES_FAMILIARES(state, payload) {
        Vue.set(state.expediente, 'antecedentes_familiares', payload.antecedentes_familiares)
    },
    SAVING_ANTECEDENTE(state, payload) {
        state.savingAntecedente = payload
    },
    UPDATE_PACIENTE(state, payload) {
        Vue.set(state.expediente, 'paciente', payload)
    },
    REMOVE_ANTECEDENTE_PATO(state, payload) {
        let index = state.expediente.antecedentes_patologicos.findIndex(function (el) {
            return el.id === payload.id
        })

        Vue.delete(state.expediente.antecedentes_patologicos, index)
    },
    REMOVE_ANTECEDENTE_NO_PATO(state, payload) {
        let index = state.expediente.antecedentes_no_patologicos.findIndex(function (el) {
            return el.id === payload.id
        })

        Vue.delete(state.expediente.antecedentes_no_patologicos, index)
    },
    REMOVE_ANTECEDENTE_FAM(state, payload) {
        let index = state.expediente.antecedentes_familiares.findIndex(function (el) {
            return el.id === payload.id
        })

        Vue.delete(state.expediente.antecedentes_familiares, index)
    },
    UPDATE_TIPO_SANGRE(state, payload) {
        state.expediente.paciente.tipo_sangre = payload.tipo_sangre
    },
    UPDATE_SERVICIO_MEDICO(state, payload) {
        state.expediente.paciente.servicio_medico = payload.servicio_medico
    },
    UPDATE_CONTACTOS_EMERGENCIA(state, payload) {
        Vue.set(state.expediente.paciente, 'contactos_emergencia', payload.contactos)
    }
}

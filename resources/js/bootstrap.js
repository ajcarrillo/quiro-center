window._ = require('lodash');

window.moment = require('moment')

window.axios = require('axios');

window.Noty = require('noty');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

Noty.overrideDefaults({
    theme: 'metroui',
});

moment.updateLocale('es', {
    months: 'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split('_'),
    weekdays: 'Domingo_Lunes_Martes_Miércoles_Jueves_Viernes_Sábado'.split('_'),
})

window.showErrorMsg = function () {
    new Noty({
        text: 'Lo sentimos ha ocurrido un error, intente de nuevo',
        type: 'error',
        timeout: 750
    }).show()
}

window.showSuccessMsg = function (msg = undefined) {
    new Noty({
        text: msg || 'La información se guardó correctamente',
        type: 'success',
        timeout: 750
    }).show()
}

window.clone = function (obj) {
    return JSON.parse(JSON.stringify(obj));
};

window.isObject = function (value) {
    return value && typeof value === 'object' && value.constructor === Object;
}

window.isEmptyObject = function (obj) {
    return (Object.getOwnPropertyNames(obj).length === 0);
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo';

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });

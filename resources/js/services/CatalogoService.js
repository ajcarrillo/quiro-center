export default {
    coloniasShow(payload) {
        return axios.get(route('api.v1.catalogos.codigos.postales'), {
            params: {
                codigo_postal: payload
            }
        })
    },
    municipiosIndex(payload) {
        return axios.get(route('api.v1.catalogos.municipios'), {
            params: {
                entidad_id: payload
            }
        })
    },
    localidesIndex(payload) {
        let {entidad_id, municipio_id} = payload

        return axios.get(route('api.v1.catalogos.localidades'), {
            params: {entidad_id, municipio_id}
        })
    }
}

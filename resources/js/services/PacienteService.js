export default {
    getPacientes() {
        return axios.get(route('api.v1.pacientes.store'))
    },
    storePaciente(form) {
        return axios.post(route('api.v1.pacientes.store'), form)
    },
    updateTipoSangre(payload) {
        let {paciente, tipo_sangre} = payload
        return axios.patch(route('api.v1.pacientes.tipo.sangre', {paciente}), {tipo_sangre})
    },
    updateServicioMedico(payload) {
        let {paciente, servicio_medico} = payload
        return axios.patch(route('api.v1.pacientes.servicio.medico', {paciente}), {servicio_medico})
    },
    updateContactosEmergencia(payload) {
        let {paciente, contactos} = payload
        return axios.patch(route('api.v1.pacientes.contactos.emergencia', {paciente}), {contactos})
    }
}

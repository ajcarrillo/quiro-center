export default {
    show(expedienteId) {
        return axios.get(route('api.v1.expedientes.show', {expediente: expedienteId}))
    },
    antecedentesPatoStore(payload) {
        return axios.post(route('api.v1.expedientes.antecedentes.patologicos.store', {expediente: payload.expedienteId}), {
            observaciones: payload.draft,
            antecedente_id: payload.antecedenteId
        })
    },
    antecedentesNoPatoStore(payload) {
        return axios.post(route('api.v1.expedientes.antecedentes.no.patologicos.store', {expediente: payload.expedienteId}), {
            observaciones: payload.draft,
            antecedente_id: payload.antecedenteId
        })
    },
    antecedentesFamiliaresStore(payload) {
        return axios.post(route('api.v1.expedientes.antecedentes.familiares.store', {expediente: payload.expedienteId}), {
            observaciones: payload.draft,
            antecedente_id: payload.antecedenteId
        })
    },
    updatePadecimientosActuales(payload) {
        let {expediente, draft} = payload

        return axios.patch(route('api.v1.expedientes.padecimientos.actuales.update', {expediente}), draft)
    },
    updateHigiene(payload) {
        let {expediente, higiene} = payload

        return axios.patch(route('api.v1.expedientes.higiene.update', {expediente}), {higiene})
    },
    updateAlimentacion(payload) {
        let {expediente, alimentacion} = payload

        return axios.patch(route('api.v1.expedientes.alimentacion.update', {expediente}), {alimentacion})
    },
    updateHabitacion(payload) {
        let {expediente, habitaciones} = payload

        return axios.patch(route('api.v1.expedientes.habitacion.update', {expediente}), {habitaciones})
    },
    updatePaciente(payload) {
        let {paciente, draft} = payload

        return axios.patch(route('api.v1.pacientes.update', {paciente}), draft)
    },
    showUltimaConsulta(payload) {
        return axios.get(route('api.v1.expedientes.ultima.consulta.show', {expediente: payload}))
    },
    destroyAntecedentePato(payload) {
        return axios.delete(route('api.v1.expedientes.antecedentes.patologicos.destroy', payload))
    },
    destroyAntecedenteNoPato(payload) {
        return axios.delete(route('api.v1.expedientes.antecedentes.no.patologicos.destroy', payload))
    },
    destroyAntecedenteFam(payload) {
        return axios.delete(route('api.v1.expedientes.antecedentes.familiares.destroy', payload))
    },
}

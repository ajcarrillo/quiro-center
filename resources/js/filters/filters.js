import Vue from 'vue'
import moment from 'moment';

Vue.filter('SiNo', val => val === false ? 'NO' : 'SI')

Vue.filter('formatDate', function (value) {
    if (value) {
        return moment(String(value)).format('DD/MM/YYYY')
    }
})

Vue.filter('formatHour', function (value) {
    if (value) {
        return moment(String(value)).format('hh:mm a')
    }
})

Vue.filter('formatDateHour', function (value) {
    if (value) {
        return moment(String(value)).format('DD/MM/YYYY hh:mm a')
    }
})

Vue.filter('formatDateMonthName', function (value) {
    if (value) {
        return moment(String(value)).format('DD/MMMM/YYYY')
    }
})

Vue.filter('dateAge', function (value) {
    if (value) {
        let today = moment();
        let birthDay = moment(String(value));

        return today.diff(birthDay, 'years');
    }
})

Vue.filter('gender', function (value) {
    if (value) {
        return value === 'M' ? 'HOMBRE' : 'MUJER'
    }
})

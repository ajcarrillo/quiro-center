const Home = () => import('../views/Home')
const NotFound = () => import('../views/NotFound')
const PacientesIndex = () => import('../views/pacientes/Index')
const PacientesCreate = () => import('../views/pacientes/Create')
const PacientesProfile = () => import('../views/pacientes/Profile')

export default {
    mode: 'history',
    routes: [
        {
            path: '*',
            component: NotFound
        },
        {
            path: '/app',
            component: Home,
            name: 'home',
            children: [
                {
                    path: 'pacientes',
                    component: Home,
                    children: [
                        {
                            path: '',
                            component: PacientesIndex,
                            name: 'pacientes-index'
                        },
                        {
                            path: 'nuevo',
                            component: PacientesCreate,
                            name: 'pacientes-create'
                        },
                        {
                            path: 'expediente/:id',
                            component: PacientesProfile,
                            name: 'pacientes-profile',
                            props: true
                        },

                    ]

                }
            ]
        }
    ]
}

@extends('layouts.app')

@section('content')
    <router-view/>
@endsection

@section('beforeScripts')
    <script type="text/javascript">
        window.antecedentesPato = {!! json_encode($antecedentesPato) !!};
        window.antecedentesNoPato = {!! json_encode($antecedentesNoPato) !!};
        window.catAntecedentesFam = {!! json_encode($catAntecedentesFam) !!};
        window.articulaciones = {!! json_encode($articulaciones) !!};
        window.musculos = {!! json_encode($musculos) !!};
        window.escolaridades = {!! json_encode($escolaridades) !!};
        window.serviciosMedicos = {!! json_encode($serviciosMedicos) !!};
    </script>
@endsection

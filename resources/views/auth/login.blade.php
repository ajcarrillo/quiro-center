@extends('layouts.app')

@section('content')
    <v-container fluid fill-height>
        <v-layout align-center justify-center>
            <v-flex xs12 sm8 md4 lg4>
                <v-card class="elevation-1 pa-3">
                    <v-card-text>
                        <div class="layout column align-center">
                            <img src="{{ asset('img/blanco_fondo_azul.png') }}" alt="Quiro Center" width="180" height="180">
                        </div>
                        <v-form method="POST" action="{{ route('login') }}">
                            @csrf
                            <v-text-field append-icon="mdi-account"
                                          name="email"
                                          label="Correo electrónico"
                                          type="text"
                            ></v-text-field>

                            <v-text-field :type="hidePassword ? 'password' : 'text'"
                                          :append-icon="hidePassword ? 'mdi-eye-off' : 'mdi-eye'"
                                          name="password"
                                          label="Contraseña"
                                          id="password"
                                          @click:append="hidePassword = !hidePassword"
                            ></v-text-field>

                            <v-btn color="primary" block type="submit">Ingresar</v-btn>
                        </v-form>
                    </v-card-text>
                </v-card>
            </v-flex>
        </v-layout>
    </v-container>
@endsection

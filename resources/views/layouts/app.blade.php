<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <style>
            [v-cloak] {
                display: none;
            }

            .theme--light.v-application {
                background: #E8EAF6 !important;
            }
        </style>
    </head>
    <body>
        <div id="app" v-cloak>
            <v-app>
                <v-app-bar app
                           flat
                           elevate-on-scroll
                           color="indigo darken-3"
                           dark
                >

                    <v-img :src="'/img/azul_fondo_gris.png'"
                           :max-width="48"
                           class="mr-4"
                    ></v-img>
                    <v-toolbar-title class="white--text">Quiro center</v-toolbar-title>

                    <v-spacer></v-spacer>
                    @auth
                        <v-menu offset-y
                                close-on-click
                                close-on-content-click
                        >
                            <template v-slot:activator="{ on }">
                                <v-btn class="white--text"
                                       v-on="on"
                                       text
                                       dark
                                >
                                    <span>Pacientes</span>
                                    <v-icon>mdi-menu-down</v-icon>
                                </v-btn>
                            </template>
                            <v-list>
                                <v-list-item :to="{name: 'pacientes-index'}" exact>
                                    <v-list-item-title>Lista</v-list-item-title>
                                </v-list-item>
                                <v-list-item :to="{name: 'pacientes-create'}" exact>
                                    <v-list-item-title>Nuevo</v-list-item-title>
                                </v-list-item>
                            </v-list>
                        </v-menu>
                        <v-btn icon
                               @click="logout"
                        >
                            <v-icon>mdi-logout</v-icon>
                        </v-btn>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    @endauth
                </v-app-bar>
                <v-content>
                    @yield('content')
                </v-content>
                <v-footer></v-footer>
            </v-app>
        </div>
        @routes
        @yield('beforeScripts')
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
